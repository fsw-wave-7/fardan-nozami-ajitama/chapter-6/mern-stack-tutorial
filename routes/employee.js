const express = require("express");
const employeeRouter = express.Router();
const Employee = require("../model/employee");

// CRUD

// read
employeeRouter.get("/", (req, res) => {
  Employee.find({}, (err, response) => {
    if (err) {
      res.status(500).json({
        message: {
          msgBody: "unable to get employee",
          msgError: true,
        },
      });
    } else {
      res.status(200).json(response);
    }
  });
});

// create
employeeRouter.post("/", (req, res) => {
  const employee = new Employee(req.body);
  employee.save((err, document) => {
    if (err) {
      res.status(500).json({
        message: {
          msgBody: "unable to add employee",
          msgError: true,
        },
      });
    } else {
      res.status(200).json({
        message: {
          msgBody: "Successfully added Employee",
          msgError: false,
        },
      });
    }
  });
});

// delete
employeeRouter.delete("/:id", (req, res) => {
  Employee.findByIdAndDelete(req.params.id, (err) => {
    if (err) {
      res.status(500).json({
        message: {
          msgBody: "unable to delete employee",
          msgError: true,
        },
      });
    } else {
      res.status(200).json({
        message: {
          msgBody: "Successfully deleted Employee",
          msgError: false,
        },
      });
    }
  });
});

// update
employeeRouter.put("/:id", (req, res) => {
  Employee.findOneAndUpdate(
    req.params.id,
    req.body,
    { runValidators: true },
    (err, response) => {
      if (err) {
        res.status(500).json({
          message: {
            msgBody: "unable to update employee",
            msgError: true,
          },
        });
      } else {
        res.status(200).json({
          message: {
            msgBody: "Successfully updated Employee",
            msgError: false,
          },
        });
      }
    }
  );
});

module.exports = employeeRouter;
